from . import models
from django.contrib import admin


registros_basicos = (
    models.Profile,
    models.Avatar,
)

for registro in registros_basicos:
    admin.site.register(registro)
