from django.conf.urls import include
from django.urls import re_path as url
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views
from . import views

app_name = "accounts"

urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^auth/', views.auth_view),
    url(r'^registration/', views.registration, name='account_signup'),
    url(r'^profile/$', login_required(views.profile), name='profile'),
    url(r'^profile/change/$', login_required(views.profile_change),
        name='profile-update'),
    url(r'^change_password/$', login_required(views.change_password),
        name='change_password'),
    url(r'^password_reset/$', views.PasswordResetView.as_view(),
        name='password_reset'),
    url(r'^password_reset/done/$', views.reset_done,
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z]+)/(?P<token>.+)/$',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm'),
    url(r'^password_reset/complete/$', views.reset_complete,
        name='password_reset_complete'),

    url("^soc/", include("social_django.urls", namespace="social")),
]
