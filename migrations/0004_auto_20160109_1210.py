# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_profile_level'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='token',
            field=models.CharField(max_length=127, null=True, editable=False),
        ),
        migrations.AddField(
            model_name='profile',
            name='token_expire',
            field=models.DateTimeField(null=True, editable=False, blank=True),
        ),
    ]
