# -*- coding: utf-8 -*-
from django.contrib import auth
from django.http import HttpResponseRedirect, HttpResponse
from django.template import loader, RequestContext
from . import forms

from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
import json
from django.urls import reverse
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth import views as auth_views

from django.conf import settings
from django.contrib.sites.models import Site


def login(request):
    if 'logout' in request.GET:
        auth.logout(request)

    if 'invalid' in request.GET:
        fail_login = True
    else:
        fail_login = False

    verbose_error = request.GET.get('verbose_error', None)

    next = request.GET.get("next", "/")
    template = "accounts/login.html"

    context = {
        'invalid': fail_login,
        'next': next,
        'verbose_error': verbose_error,
    }
    if hasattr(settings, 'SOCIAL_AUTH_GOOGLE_OAUTH2_KEY'):
        context['google_key'] = settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY

    if hasattr(settings, 'SOCIAL_AUTH_FACEBOOK_KEY'):
        context['facebook_key'] = settings.SOCIAL_AUTH_FACEBOOK_KEY

    return render(request, template, context)


def auth_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    next = request.POST.get('next', '/')
    user = auth.authenticate(username=username, password=password)
    if user is not None:
        auth.login(request, user)
        return HttpResponseRedirect(next)
    else:
        return HttpResponseRedirect('/accounts/login/?invalid=True')


def registration(request):
    args = {}
    if request.method == 'POST':
        form = forms.RegistrationForm(request.POST)     # create form object
        args['form'] = form
        if form.is_valid():
            form.save()
            user = auth.authenticate(username=request.POST['username'],
                                     password=request.POST['password1'])

            auth.login(request, user)
            return HttpResponseRedirect('/')
    else:
        args['form'] = forms.RegistrationForm()
    args['active_form'] = "signup-box"
    return render(request, 'accounts/login.html', args)


def profile(request):
    username = request.GET.get("username", request.user.username)

    user = auth.models.User.objects.get(username=username)

    friends = auth.models.User.objects.all()

    template = loader.get_template("accounts/profile.html")
    context = {
        'usuario': user,
        'friends': friends,
    }
    return HttpResponse(template.render(context, request))


def profile_change(request):
    content = {"status": "FAILURE"}
    if request.method == "POST":
        value = request.POST.get("value", None)
        if value:
            value = value.strip()
        else:
            value = None
        name = request.POST.get("name", None)

        user = request.user
        if name == "first_name" and value:
            user.first_name = value
        elif name == "last_name":
            user.last_name = value
        elif name == "email":
            user.email = value
        elif name == "username":
            user.username = value
        elif name == "level":
            user.username = value
        elif name == "picture":
            user.profile.picture = request.FILES["avatar"]
            user.profile.save()
            content["picture_url"] = user.profile.picture.url
        else:
            raise ObjectDoesNotExist
        user.save()
        content["status"] = "OK"

    response = HttpResponse(content_type='application/json; charset=utf-8',
                            content=json.dumps(content))
    return response


def change_password(request):
    errors = list()
    user = None
    if request.method == 'POST':
        password = request.POST.get("password", None)
        repassword = request.POST.get("repassword", None)
        user = request.user
        if password and password == repassword:
            user.set_password(password)
            user.save()
            return HttpResponseRedirect('/accounts/profile/?pasword_changed=1')
        else:
            errors.append("Las conraseñas no coinciden")

    template = loader.get_template("accounts/password.html")
    user = request.user
    context = {
        'usuario': user,
        'errors': errors,
    }
    return HttpResponse(template.render(context, request))


def reset_done(request):
    template = loader.get_template("accounts/password_reset_done.html")
    return HttpResponse(template.render({'site': Site.objects.get_current()}, request))

def reset_complete(request):
    template = loader.get_template("accounts/password_reset_complete.html")
    return HttpResponse(template.render({'site': Site.objects.get_current() }, request))

class PasswordResetView(auth_views.PasswordResetView):

    template_name = 'accounts/login.html'
    subject_template_name = 'registration/password_reset_subject.txt'
    extra_context = {'active_form': "forgot-box"}
    email_template_name = None
    html_email_template_name = 'accounts/reset_email.html'
    email_template_name = 'accounts/reset_email.txt'
    success_url = "done/"


class PasswordResetConfirmView(auth_views.PasswordResetConfirmView):
    post_reset_login = True
    success_url = "confirm/"
