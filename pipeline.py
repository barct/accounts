# -*- coding: utf-8 -*-
from .models import Profile
import urllib
from django.core.files import File
import os
from django.utils import timezone
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.urls import reverse
import tempfile
import requests
from django.core import files


def get_avatar(backend, strategy, details, response,
               user=None, *args, **kwargs):
    if not hasattr(user, 'profile'):
        Profile.objects.create(user=user)
        Profile.objects.filter(user__id=user.id).update(
            last_activity=timezone.now())
    if not user.profile.picture:
        url = None
        if backend.name == 'facebook':
            try: 
                url = "http://graph.facebook.com/%s/picture?type=large" % (
                    response['id'])
            except:
                url = None
        if backend.name == 'twitter':
            url = response.get('profile_image_url', '').replace('_normal', '')
        if backend.name == 'google-oauth2':
            try:
                url = response['picture'].get('url')
                url = url.split('?')[0]
            except:
                url = None
        if url:
            response = requests.get(url, stream=True)
            # Was the request OK?
            if response.status_code == requests.codes.ok:

                # Nope, error handling, skip file etc etc etc
                # Get the filename from the url, used for saving later
                file_name = url.split('/')[-1]

                # Create a temporary file
                lf = tempfile.NamedTemporaryFile()

                # Read the streamed image in sections
                for block in response.iter_content(1024 * 8):
                    # If no more file then stop
                    if not block:
                        break

                    # Write image block to temporary file
                    lf.write(block)
                # Save the temporary image to the model#
                # This saves the model so be sure that it is valid
                user.profile.picture.save(file_name, files.File(lf))


def block_create_user(backend, strategy, details, response,
                      user=None, *args, **kwargs):
    if user:
        return {'is_new': False}
    else:
        users = User.objects.filter(email=details['email'])
        if users.count() > 0:
            return {
                'is_new': True,
                'user': users[0]
            }
        else:
            return redirect(
                reverse('accounts:login') + u"?invalid=True&verbose_error="
                u"No se encuentra registrado, por favor regístrese o solicite"
                u" la registración a su administrador")
