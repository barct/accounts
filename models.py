# -*- encoding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.contrib.sites.models import Site
try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.utils.crypto import get_random_string
from django.db.models import Q
from django.dispatch import Signal
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.contrib.sites.models import Site


def strfdelta(tdelta, fmt):
    d = {"days": tdelta.days}
    d["hours"], rem = divmod(tdelta.seconds, 3600)
    d["minutes"], d["seconds"] = divmod(rem, 60)
    return fmt.format(**d)


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE)
    last_activity = models.DateTimeField(
        "última actividad", null=True, blank=True)
    picture = models.ImageField(upload_to='profiles', null=True, blank=True)
    avatar = models.ForeignKey(
        'Avatar', on_delete=models.SET_NULL, null=True, blank=True)
    level = models.IntegerField(default=0)

    def verbose_last_activity(self):
        if self.last_activity:
            delta = timezone.now() - self.last_activity  # epoch seconds
            secs = delta.total_seconds()
            if secs < 60 * 5:
                return "hace momentos"
            elif secs < 60 * 60:
                return "hace un rato"
            else:
                return strfdelta(delta, "{days} días, {hours} horas")
        else:
            return "nunca"

    def save(self, *args, **kwargs):
        if not self.avatar:
            self.avatar = Avatar.objects.order_by("?").first()
        if self.pk is None:
            new = True
        else:
            new = False
        obj = super(Profile, self).save(*args, **kwargs)
        if new:
            Invitation.check_invitations(self)
        return obj

    def is_online(self):
        if self.last_activity:
            delta = timezone.now() - self.last_activity
            if delta.total_seconds() < 60 * 5:
                return True
        return False

    @property
    def full_name(self):
        return "%s %s" % (self.user.first_name, self.user.last_name)

    def __str__(self):
        return self.full_name

    def __str__(self):
        return self.full_name

    def get_profile_avatar(self):

        if not self.picture:
            if not self.avatar:
                pics = Avatar.objects.all()
                if len(pics) > 0:
                    return pics[0].picture
                else:
                    return False
            else:
                return self.avatar.picture
        else:
            return self.picture


class Avatar(models.Model):
    picture = models.ImageField(upload_to='profiles')

    def __str__(self):
        return self.picture.url

    def __str__(self):
        return self.picture.url


class Invitation(models.Model):
    accepted = models.BooleanField(verbose_name=_('accepted'), default=False)
    key = models.CharField(verbose_name=_('key'), max_length=64, unique=True)
    sent = models.DateTimeField(verbose_name=_('sent'), null=True)
    inviter = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.CASCADE)  # noqa
    email = models.EmailField(verbose_name=_(
        'e-mail address'), max_length=127)
    full_name = models.CharField(max_length=127, null=True, blank=True)
    created = models.DateTimeField(verbose_name=_('created'),
                                   default=timezone.now)
    extra_data = models.TextField(null=True, blank=True)

    @classmethod
    def create(cls, email=None, full_name=None, inviter=None,
               extra_data=None, **kwargs):
        key = get_random_string(64).lower()
        instance = cls._default_manager.create(
            email=email,
            key=key,
            inviter=inviter,
            extra_data=extra_data,
            full_name=full_name,
            **kwargs)
        return instance

    @classmethod
    def check_invitations(cls, profile):
        invitations = Invitation.objects.filter(
            email=profile.user.email, accepted=False)
        for i in invitations:
            invite_profile_create.send(
                sender=cls, profile=profile, invitation=i)
            i.accepted = True
            i.save()

    def send_invitation(self, request, **kwargs):
        current_site = kwargs.pop('site', Site.objects.get_current())
        invite_url = reverse('accounts:login')
        invite_url = request.build_absolute_uri(invite_url)
        ctx = kwargs
        ctx.update({
            'invite_url': invite_url,
            'site_name': current_site.name,
            'email': self.email,
            'key': self.key,
            'inviter': self.inviter,
        })

        email_template = 'accounts/email_invite.html'
        subject = 'Invitación para unirse a nuestro sitio'
        to = (self.email, )
        message = get_template(email_template).render(ctx, request)
        msg = EmailMessage(subject, message, to=to)
        msg.content_subtype = 'html'
        msg.send()


invite_profile_create = Signal()
