from django import forms
# fill in custom user info then save it
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.conf import settings
from django_recaptcha.fields import ReCaptchaField


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    nombres = forms.CharField(required=False)
    apellidos = forms.CharField(required=False)
    captcha = ReCaptchaField(required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        if (hasattr(settings, "ACCOUNTS_REGISTER_SECRET") and
                settings.ACCOUNTS_REGISTER_SECRET):
            self.fields["secret"] = forms.CharField(
                label='Frase Secreta', required=True)

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['nombres']
        user.last_name = self.cleaned_data['apellidos']
        if commit:
            user.save()

        return user

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).count() > 0:
            raise ValidationError("Este email ya esta registrado")
        return email

    def clean_secret(self):
        secret = self.cleaned_data['secret']
        if secret != settings.ACCOUNTS_REGISTER_SECRET:
            raise ValidationError("No acertaste a la palabra secreta")
        return secret
